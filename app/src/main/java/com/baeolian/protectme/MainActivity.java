package com.baeolian.protectme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class MainActivity extends AppCompatActivity {

    Switch sw;
    String masterKeyAlias;
    EncryptedSharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sw = findViewById(R.id.swt);
        if(sw.isChecked()){
            try {
                masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                sp = (EncryptedSharedPreferences) EncryptedSharedPreferences.create(
                        "protected_sp",
                        masterKeyAlias,
                        this,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                    );
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            int taps = sp.getInt("contador", 1);
            updateText(taps + " \u20BF\n", true);
        } else{
            SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
            int taps = sharedPreferences.getInt("contador", 1);
            updateText(taps + " \u20BF\n", false);
        }
    }

    public void tap1(View v) {
        if(sw.isChecked()) {
            try {
                masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                sp = (EncryptedSharedPreferences) EncryptedSharedPreferences.create(
                        "protected_sp",
                        masterKeyAlias,
                        this,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                );
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            SharedPreferences.Editor editor = sp.edit();
            int taps = sp.getInt("contador", 1);
            int res = taps + 1;
            editor.putInt("contador", res);
            editor.apply();
            updateText(res + " \u20BF\n", true);
        } else{
            SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
            final SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
            int taps = sharedPreferences.getInt("contador", 1);
            int res = taps + 1;
            sharedEditor.putInt("contador", res);
            sharedEditor.apply();
            updateText(res + " \u20BF\n", false);
        }
    }

    public void tap32(View v) {
        if(sw.isChecked()) {
            try {
                masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                sp = (EncryptedSharedPreferences) EncryptedSharedPreferences.create(
                        "protected_sp",
                        masterKeyAlias,
                        this,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                );
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            SharedPreferences.Editor editor = sp.edit();
            int taps = sp.getInt("contador", 1);
            int res = taps + 32;
            editor.putInt("contador", res);
            editor.apply();
            updateText(res + " \u20BF\n", true);
        } else {
            SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
            final SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
            int taps = sharedPreferences.getInt("contador", 1);
            int res = taps + 32;
            sharedEditor.putInt("contador", res);
            sharedEditor.apply();
            updateText(res + " \u20BF\n", false);
        }
    }

    public void resetCounter(View v){
        if(sw.isChecked()) {
            try {
                masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                sp = (EncryptedSharedPreferences) EncryptedSharedPreferences.create(
                        "protected_sp",
                        masterKeyAlias,
                        this,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                );
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt("contador", 0);
            editor.apply();
            updateText("0 \u20BF\n", true);
        } else {
            SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
            final SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
            sharedEditor.putInt("contador", 0);
            sharedEditor.apply();
            updateText("0 \u20BF\n", false);
        }
    }

    private void updateText(String cnt, Boolean state){
        TextView contador = findViewById(R.id.counter);
        if(state){
            contador.setTextColor(Color.GREEN);
        } else{
            contador.setTextColor(Color.RED);
        }
        contador.setText(cnt);
    }
}
